import React from "react"
import PropTypes from "prop-types"
import { Form, Button } from "semantic-ui-react"

class ModalFormRemove extends React.Component {
    state = {
        data: {
            recordId: ""
        }
    }

    onChange = e => {
        this.setState({
            ...this.state,
            data: { ...this.state.data, [e.target.name]: e.target.value }
        })
    }

    onSubmit = () => {
        this.props.submit(this.state.data)
    }

    render() {
        const { data } = this.state

        return (
            <Form onSubmit={this.onSubmit} noValidate width="equal">
                <Form.Field>
                    <label htmlFor="field">Record Id</label>
                    <input
                        type="text"
                        id="recordId"
                        name="recordId"
                        autoComplete="true"
                        value={data.recordId}
                        onChange={this.onChange}
                    />
                </Form.Field>

                <Button positive>Remove</Button>
            </Form>
        )
    }
}

ModalFormRemove.propTypes = {
    submit: PropTypes.func.isRequired
}

export default ModalFormRemove
