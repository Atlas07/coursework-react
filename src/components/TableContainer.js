import React from "react"
import PropTypes from "prop-types"
import { Table } from "semantic-ui-react"

import TableRow from "./TableRow"

const TableContainer = ({ table }) => (
    <div className="table-container">
        <Table celled selectable>
            <Table.Header>
                <Table.Row>
                    {table.length !== 0 &&
                        Object.keys(table[0]).map(item => (
                            <Table.HeaderCell key={Math.random()}>
                                {item}
                            </Table.HeaderCell>
                        ))}
                </Table.Row>
            </Table.Header>

            <Table.Body>
                {table.length !== 0 &&
                    table.map(item => (
                        <TableRow row={item} key={Math.random()} />
                    ))}
            </Table.Body>
        </Table>
    </div>
)

TableContainer.propTypes = {
    table: PropTypes.arrayOf({}).isRequired
}

export default TableContainer
