import React from "react"
import { Table } from "semantic-ui-react"

const TableRow = ({ row }) => {
    return (
        <Table.Row>
            {Object.keys(row).map(item => (
                <Table.Cell key={Math.random()}>{row[item]}</Table.Cell>
            ))}
        </Table.Row>
    )
}

export default TableRow
