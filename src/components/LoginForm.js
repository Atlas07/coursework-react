import React from 'react'
import PropTypes from 'prop-types'
import isEmail from 'validator/lib/isEmail'
import isNumeric from 'validator/lib/isNumeric'
import { Form, Button, Message } from 'semantic-ui-react'

import InlineError from "./InlineError"

class LoginForm extends React.Component {
    state = {
        data: {
            email: '',
            password: ''
        },
        loading: false,
        errors: {}
    }

    onChange = e => {
        this.setState({
            ...this.state,
            data: { ...this.state.data, [e.target.name]: e.target.value }
        })
    }

    onSubmit = () => {
        const errors = this.validate(this.state.data)

        this.setState({ errors })

        if (Object.keys(errors).length === 0) {
            this.setState({ loading: true })
            this.props.submit(this.state.data).catch(() => {
                errors.global = 'Something went wrong'
                this.setState({ loading: false, errors })
            })
        }
    }

    validate = data => {
        const errors = {}

        if (!isEmail(data.email)) {
            errors.email = "Invalid email"
        }
        if (!data.password) {
            errors.password = "Can't be blank"
        }
        if (!isNumeric(data.password)) {
            errors.password = "Have to be number"
        }

        return errors
    }

    render() {
        const { data, errors, loading } = this.state

        return (
            <Form onSubmit={this.onSubmit} noValidate loading={loading}>

                {!!errors.global && (
                    <Message negative>
                        <Message.Header>Something went wrong</Message.Header>
                    </Message>
                )}

                <Form.Field error={!!errors.email}>
                    <label htmlFor="email">Email</label>
                    <input
                        type="text"
                        id="email"
                        name="email"
                        placeholder="example@example.com"
                        autoComplete="true"
                        value={data.email}
                        onChange={this.onChange}
                    />
                    {errors.email && <InlineError text={errors.email} />}
                </Form.Field>

                <Form.Field error={errors.password}>
                    <label htmlFor="password">Password</label>
                    <input
                        type="password"
                        id="password"
                        name="password"
                        autoComplete="true"
                        value={data.password}
                        onChange={this.onChange}
                    />
                    {errors.password && <InlineError text={errors.password} />}
                </Form.Field>

                <Button primary>Login</Button>

            </Form>
        )
    }
}

LoginForm.propTypes = {
    submit: PropTypes.func.isRequired,
}

export default LoginForm