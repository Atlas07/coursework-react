import React from "react"
import PropTypes from "prop-types"
import { Segment } from "semantic-ui-react"

const UserContainer = ({ info }) => (
    <div>
        <Segment>Name: {info.name}</Segment>
        <Segment>Surname: {info.surname}</Segment>
        <Segment>Phone: {info.phone}</Segment>
        <Segment>Email: {info.email}</Segment>
        <Segment>Position: {info.position_name}</Segment>
    </div>
)

UserContainer.propTypes = {
    info: PropTypes.shape({
        name: PropTypes.string.isRequired,
        surname: PropTypes.string.isRequired,
        email: PropTypes.string.isRequired,
        phone: PropTypes.string.isRequired
    }).isRequired
}
export default UserContainer
