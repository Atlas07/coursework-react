import React from "react"
import PropTypes from "prop-types"
import { Form, Button } from "semantic-ui-react"

class ModalFormEdit extends React.Component {
    state = {
        data: {
            id: undefined
        },
        record: undefined
    }

    onChange = e => {
        this.setState({
            data: { ...this.state.data, [e.target.name]: e.target.value }
        })
    }

    onSubmit = () => {
        this.props.submit(this.state.data)
    }

    onFindData = () => {
        let id = ""

        Object.keys(this.props.formStructure).forEach(key => {
            if (key.indexOf("id") !== -1) {
                id = key
            }
        })

        this.props.table.forEach(record => {
            if (+record[id] === +this.state.data.id) {
                this.setState({ data: record })
            }
        })
    }

    render() {
        const { data } = this.state
        const { formStructure } = this.props

        return (
            <div>
                <Form onSubmit={this.onFindData} noValidate>
                    <Form.Group inline>
                        <Form.Field>
                            <label htmlFor="id">Id</label>
                            <input
                                type="text"
                                id="id"
                                name="id"
                                placeholder="Type id record"
                                autoComplete="true"
                                value={data.id}
                                onChange={this.onChange}
                            />
                        </Form.Field>
                        <Form.Button positive className="id-button">
                            Find record
                        </Form.Button>
                    </Form.Group>
                </Form>

                <Form onSubmit={this.onSubmit} noValidate>
                    {Object.keys(formStructure).map(field => {
                        if (field.indexOf("id") !== -1) return <span />

                        return (
                            <Form.Field>
                                <label htmlFor="field">{field}</label>
                                <input
                                    type="text"
                                    id={field}
                                    name={field}
                                    autoComplete="true"
                                    value={data[field]}
                                    onChange={this.onChange}
                                />
                            </Form.Field>
                        )
                    })}

                    <Button positive>Edit</Button>
                </Form>
            </div>
        )
    }
}

ModalFormEdit.propTypes = {
    submit: PropTypes.func.isRequired,
    formStructure: PropTypes.shape({}).isRequired,
    table: PropTypes.arrayOf().isRequired
}

export default ModalFormEdit
