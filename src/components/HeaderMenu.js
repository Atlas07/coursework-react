import React from "react"
import PropTypes from "prop-types"
import { Menu } from "semantic-ui-react"

const HeaderMenu = ({ getInfo, logout, user }) => (
    <Menu className="dashboard-top-mnu">
        <Menu.Item header>Dashboard</Menu.Item>
        <Menu.Item name="departments" onClick={getInfo}>
            Departments
        </Menu.Item>
        <Menu.Item name="courses" onClick={getInfo}>
            Courses
        </Menu.Item>
        <Menu.Item name="positions" onClick={getInfo}>
            Positions
        </Menu.Item>
        {!!user.name && (
            <Menu.Item position="right">
                {user.name} {user.surname}
            </Menu.Item>
        )}
        <Menu.Item onClick={logout} className="logout-btn">
            Logout
        </Menu.Item>
    </Menu>
)

HeaderMenu.propTypes = {
    getInfo: PropTypes.func.isRequired,
    logout: PropTypes.func.isRequired,
    user: PropTypes.shape({
        name: PropTypes.string,
        surname: PropTypes.string
    })
}

export default HeaderMenu
