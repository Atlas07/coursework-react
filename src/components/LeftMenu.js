import React from "react"
import PropTypes from "prop-types"
import { Menu, Button, Modal } from "semantic-ui-react"

import ModalFormAdd from "./ModalFormAdd"
import ModalFormRemove from "./ModalFormRemove"
import ModalFormEdit from "./ModalFormEdit"

const LeftMenu = ({
    getInfo,
    createRecord,
    updateRecord,
    removeRecord,
    formStructure,
    table,
    isAdd,
    isManager
}) => {
    const AddButton = (
        <Button positive className="record-control-btn" disabled={isAdd}>
            Add
        </Button>
    )

    const RemoveButton = (
        <Button negative className="record-control-btn" disabled={isAdd}>
            Remove
        </Button>
    )

    const EditButton = (
        <Button color="yellow" className="record-control-btn" disabled={isAdd}>
            Edit
        </Button>
    )

    return (
        <div>
            <Menu vertical className="dashboard-vertical-mnu">
                <Menu.Item onClick={() => getInfo("info")}>My info</Menu.Item>
                <Menu.Item onClick={() => getInfo("courses")}>
                    Courses
                </Menu.Item>
                <Menu.Item onClick={() => getInfo("trips")}>
                    Business trips
                </Menu.Item>
                <Menu.Item onClick={() => getInfo("sicknessList")}>
                    Sickness list
                </Menu.Item>
            </Menu>

            {isManager && (
                <Modal trigger={AddButton} closeIcon>
                    <Modal.Header>Add new record</Modal.Header>
                    <Modal.Content>
                        <ModalFormAdd
                            submit={createRecord}
                            formStructure={formStructure}
                        />
                    </Modal.Content>
                </Modal>
            )}

            {isManager && (
                <Modal trigger={RemoveButton} closeIcon>
                    <Modal.Header>Remove record</Modal.Header>
                    <Modal.Content>
                        <ModalFormRemove submit={removeRecord} />
                    </Modal.Content>
                </Modal>
            )}

            {isManager && (
                <Modal trigger={EditButton} closeIcon>
                    <Modal.Header>Edit record</Modal.Header>
                    <Modal.Content>
                        <ModalFormEdit
                            submit={updateRecord}
                            formStructure={formStructure}
                            table={table}
                        />
                    </Modal.Content>
                </Modal>
            )}
        </div>
    )
}

LeftMenu.propTypes = {
    getInfo: PropTypes.func.isRequired,
    createRecord: PropTypes.func.isRequired,
    updateRecord: PropTypes.func.isRequired,
    removeRecord: PropTypes.func.isRequired,
    isAdd: PropTypes.bool.isRequired,
    isManager: PropTypes.bool.isRequired
}

export default LeftMenu
