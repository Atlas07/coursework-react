import React from "react"
import PropTypes from "prop-types"
import { Form, Button } from "semantic-ui-react"

class ModalFormAdd extends React.Component {
    state = {
        data: {}
    }

    onChange = e => {
        this.setState({
            data: { ...this.state.data, [e.target.name]: e.target.value }
        })
    }

    onSubmit = () => {
        this.props.submit(this.state.data)
    }

    render() {
        const { data } = this.state
        const { formStructure } = this.props

        return (
            <Form onSubmit={this.onSubmit} noValidate>
                {Object.keys(formStructure).map(field => {
                    if (field.indexOf("id") !== -1) return <span />

                    return (
                        <Form.Field>
                            <label htmlFor="field">{field}</label>
                            <input
                                type="text"
                                id={field}
                                name={field}
                                autoComplete="true"
                                value={data.field}
                                onChange={this.onChange}
                            />
                        </Form.Field>
                    )
                })}

                <Button positive>Add</Button>
            </Form>
        )
    }
}

ModalFormAdd.propTypes = {
    submit: PropTypes.func.isRequired,
    formStructure: PropTypes.shape({}).isRequired
}

export default ModalFormAdd
