const handleResponse = res => {
    if (res.ok || res.status === 200 || res.status === 201) {
        return res.data
    }

    const error = new Error(res.statusText)
    error.response = res
    throw error
}

export { handleResponse }