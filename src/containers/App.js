import React from 'react'
import PropTypes from 'prop-types'
import { hot } from "react-hot-loader"
import { Route } from "react-router-dom"

import LoginPage from './LoginPage'
import Dashboard from './Dashboard'

const App = ({ location }) => (
    <div className="ui-container">
        <Route path="/" exact component={LoginPage} />
        <Route location={location} exact path="/login" component={LoginPage} />
        <Route location={location} exact path="/dashboard" component={Dashboard} />
    </div>
)

App.propTypes = {
    location: PropTypes.shape({
        href: PropTypes.string
    }).isRequired
}

export default hot(module)(App)