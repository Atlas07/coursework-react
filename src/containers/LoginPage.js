import React from 'react'
import PropTypes from 'prop-types'
import axios from 'axios'
import { Grid } from "semantic-ui-react"

import { handleResponse } from '../helpers'
import LoginForm from '../components/LoginForm'

class LoginPage extends React.Component {
    state = {}



    submit = data =>
        axios
            .post("http://localhost:3000/login", data)
            .then(handleResponse)
            .then(res => {
                localStorage.setItem('userToken', res.employee_id)
                this.props.history.push('/dashboard')
            })

    render() {
        return (
            <div className="login-page">
                <Grid centered>
                    <Grid.Row>
                        <Grid.Column mobile={14} tablet={10} widescreen={6} largeScreen={6}>
                            <h1 className="login-h1">Login Page</h1>
                            <LoginForm submit={this.submit} />
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div>
        );
    }
}

LoginPage.propTypes = {
    history: PropTypes.shape({
        push: PropTypes.func.isRequired
    }).isRequired
}

export default LoginPage