import React from "react"
import PropTypes from "prop-types"
import axios from "axios"
import { Grid, Loader } from "semantic-ui-react"

import { handleResponse } from "../helpers"

import HeaderMenu from "../components/HeaderMenu"
import LeftMenu from "../components/LeftMenu"
import TableContainer from "../components/TableContainer"

import "../main.css"

class Dashboard extends React.Component {
    state = {
        table: [],
        user: {
            id: localStorage.getItem("userToken")
        },
        loading: false,
        isLeftMenuActive: false,
        activeTable: ""
    }

    componentDidMount() {
        this.setState({ loading: true })

        axios
            .post("http://localhost:3000/user", { id: this.state.user.id })
            .then(handleResponse)
            .then(res => {
                this.setState({
                    user: {
                        ...this.state.user,
                        ...res
                    },
                    loading: false
                })
            })
    }

    getMenuInfo = (e, { name }) => {
        this.setState({ loading: true })

        axios
            .post("http://localhost:3000/table", { table: name })
            .then(handleResponse)
            .then(res => {
                this.setState({
                    table: res,
                    loading: false,
                    isLeftMenuActive: false,
                    activeTable: name
                })
            })
    }

    getUserInfo = fieldName => {
        this.setState({ loading: true })

        axios
            .post(`http://localhost:3000/user-info`, {
                id: this.state.user.id,
                tab: fieldName
            })
            .then(handleResponse)
            .then(res => {
                this.setState({
                    table: res,
                    loading: false,
                    isLeftMenuActive: true
                })
            })
    }

    createRecord = data => {
        axios
            .post(`http://localhost:3000/add`, {
                table: this.state.activeTable,
                data
            })
            .then(handleResponse)
            .then(() => {
                this.getMenuInfo(null, { name: this.state.activeTable })
            })
    }

    updateRecord = data => {
        let idName = ""
        let id

        Object.keys(data).forEach(key => {
            if (key.indexOf("id") !== -1) {
                idName = key
                id = data[key]
            }
        })

        delete data[idName]

        axios
            .post(`http://localhost:3000/update`, {
                table: this.state.activeTable,
                data,
                id
            })
            .then(handleResponse)
            .then(() => {
                this.getMenuInfo(null, { name: this.state.activeTable })
            })
    }

    removeRecord = ({ recordId }) => {
        axios
            .post(`http://localhost:3000/remove`, {
                id: recordId,
                table: this.state.activeTable
            })
            .then(handleResponse)
            .then(() => {
                this.getMenuInfo(null, { name: this.state.activeTable })
            })
    }

    logout = () => {
        localStorage.removeItem("userToken")
        this.props.history.push("/login")
    }

    render() {
        const { table, user, loading, isLeftMenuActive } = this.state

        if (!user.id) {
            this.logout()
        }

        return (
            <Grid>
                <Grid.Row>
                    <Grid.Column widescreen={16}>
                        <HeaderMenu
                            user={user}
                            getInfo={this.getMenuInfo}
                            logout={this.logout}
                        />
                    </Grid.Column>
                    <Grid.Column widescreen={4}>
                        <LeftMenu
                            getInfo={this.getUserInfo}
                            createRecord={this.createRecord}
                            removeRecord={this.removeRecord}
                            updateRecord={this.updateRecord}
                            formStructure={table[0]}
                            table={table}
                            isAdd={isLeftMenuActive || table.length === 0}
                            isManager={!!user.position_is_manager}
                        />
                    </Grid.Column>

                    <Grid.Column widescreen={10}>
                        {<TableContainer table={table} />}
                    </Grid.Column>
                </Grid.Row>
                <Loader active={loading} />
            </Grid>
        )
    }
}

Dashboard.propTypes = {
    history: PropTypes.shape({
        push: PropTypes.func.isRequired
    }).isRequired
}

export default Dashboard
